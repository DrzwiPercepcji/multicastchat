﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace MultiChat
{
    class Member
    {
        public string nick;
        public long timeout;
        public Color color;

        public Member(string nick, Color color)
        {
            this.nick = nick;
            this.color = color;

            UpdateTimeout();
        }

        public void UpdateTimeout()
        {
            timeout = DateTimeOffset.Now.ToUnixTimeSeconds() + 5;
        }
    }
}
