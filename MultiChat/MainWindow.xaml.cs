﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;

namespace MultiChat
{
    public partial class MainWindow : Window
    {
        private const int PORT = 6024;
        private const string ADDRESS = "239.0.0.222";

        private int colorIndex = 0;

        Dictionary<string, Member> members;

        private UdpClient sender;
        private UdpClient receiver;

        private IPAddress address;
        private IPEndPoint IPEndPoint;

        private Thread receiveThread;
        private Thread aliveThread;

        private Color[] colors;

        private bool alive = true;
        private const int maxMessages = 7;
        private string username;

        public delegate void DelegeteList(IPEndPoint endPoint, byte[] data);

        public DelegeteList delegeteList;

        public MainWindow(string username)
        {
            InitializeComponent();
            Closed += new EventHandler(MainWindow_Closed);

            this.username = username;

            members = new Dictionary<string, Member>();

            CreateConnection();

            colors = new Color[10];
            colors[0] = Colors.Blue;
            colors[1] = Colors.Red;
            colors[2] = Colors.Green;
            colors[3] = Colors.Pink;
            colors[4] = Colors.Navy;
            colors[5] = Colors.Yellow;
        }

        private void CreateConnection()
        {
            address = IPAddress.Parse(ADDRESS);
            IPEndPoint = new IPEndPoint(address, PORT);

            IPEndPoint tempIPEndPoint = new IPEndPoint(IPAddress.Any, PORT);

            try
            {
                sender = new UdpClient(AddressFamily.InterNetwork);
                receiver = new UdpClient(AddressFamily.InterNetwork);

                receiver.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
                receiver.Client.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.MulticastLoopback, true);

                receiver.Client.Bind(tempIPEndPoint);
            }
            catch (SocketException e)
            {
                Console.WriteLine("Error: " + e.Message);
            }

            try
            {
                sender.JoinMulticastGroup(address, 50);
                receiver.JoinMulticastGroup(address, 50);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }

            delegeteList = new DelegeteList(ParseReceivedData);
            alive = true;

            receiveThread = new Thread(ReceiveThreadProcess);
            receiveThread.Start();

            aliveThread = new Thread(AliveThreadProcess);
            aliveThread.Start();
        }

        private void MainWindow_Closed(object sender, EventArgs e)
        {
            alive = false;

            this.sender.DropMulticastGroup(address);
            this.sender.Close();

            this.receiver.DropMulticastGroup(address);
            this.receiver.Close();
        }

        public void AddMessage(string member, string message)
        {
            ListBoxItem item = new ListBoxItem();
            item.Content = members[member].nick + ": " + message;
            item.Foreground = new SolidColorBrush(members[member].color);

            Messages.Items.Add(item);

            if (Messages.Items.Count > maxMessages)
            {
                Messages.Items.RemoveAt(0);
            }
        }

        private void ReceiveThreadProcess()
        {
            ReceiveMessage();
        }

        private void ReceiveMessage()
        {
            byte[] data;

            while (alive)
            {
                IPEndPoint tempEndPoint = new IPEndPoint(IPAddress.Any, 0);

                if (receiver.Available > 0)
                {
                    data = receiver.Receive(ref tempEndPoint);

                    Dispatcher.Invoke(delegeteList, tempEndPoint, data);
                }
                else
                {
                    Thread.Sleep(10);
                }
            }
        }

        private void ParseReceivedData(IPEndPoint endPoint, byte[] data)
        {
            string content = Encoding.Unicode.GetString(data, 0, data.Length);

            string[] parts = content.Split(':');

            if (parts.Length < 2)
                return;

            string action = parts[0];
            string message = string.Join(":", parts.Skip(1));

            string member = endPoint.Address + ":" + endPoint.Port;

            if (action == "alive")
            {
                UpdateMember(member, message);
                CheckAlive();
            }

            if (action == "message")
            {
                AddMessage(member, message);
            }
        }

        private void UpdateMember(string address, string member)
        {
            long time = DateTimeOffset.Now.ToUnixTimeSeconds() + 5;

            if (members.ContainsKey(address) == true)
            {
                members[address].UpdateTimeout();
            }
            else
            {
                Member temp = new Member(member, colors[colorIndex]);
                members.Add(address, temp);

                colorIndex++;
            }
        }

        private void AliveThreadProcess()
        {
            SendAlive();
        }

        private void SendAlive()
        {
            Send("alive:" + username);
            Thread.Sleep(500);

            if(alive)
                SendAlive();
        }

        private void CheckAlive()
        {
            Dictionary<string, Member> temp = new Dictionary<string, Member>(members);

            Members.Items.Clear();

            foreach (KeyValuePair<string, Member> entry in temp)
            {
                long now = DateTimeOffset.Now.ToUnixTimeSeconds();

                if (entry.Value.timeout < now)
                {
                    members.Remove(entry.Key);
                }
                else
                {
                    ListBoxItem item = new ListBoxItem();
                    item.Content = members[entry.Key].nick;
                    item.Foreground = new SolidColorBrush(entry.Value.color);

                    Members.Items.Add(item);
                }
            }
        }

        public void UpdateMembersList(List<ListBoxItem> items)
        {
            Members.Items.Add(items);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            string message = Input.Text;

            Send("message:" + message);

            Console.WriteLine("Message was sent to " + IPEndPoint.Address.ToString() + " on their port number " + IPEndPoint.Port.ToString());
        }

        private void Send(string content)
        {
            if (content == null)
                return;

            byte[] buffer = Encoding.Unicode.GetBytes(content);

            this.sender.Send(buffer, buffer.Length, IPEndPoint);
        }
    }
}